# base16-contra-scheme

Contra - [base16](https://github.com/chriskempson/base16) WCAG 2.0 contrast ratio based color scheme

It's all about readability.

## Colors
![palette](palette.png)

## About

__tl;dr__ - bigger difference in colors contrast ratio = easier for the brain to process the difference


The main idea of this scheme is - highly readable text.
Our brain mainly relies on the contrasts to evaluate colors.
In other words, if we take two distinct colors and use them in the foreground/background combination we can get something like this:

![full saturation](pink-yellow-full-s.png)

Looks aweful... No matter the fact that the colors are much different in Hue, it is hard to read the text.
If we remove the saturation and leave the color pair in a gray-scale variant, the contrast ratio between the colors is low. And we see exactly why these two colors make a bad pair.

![no saturation](pink-yellow-no-s.png)

The colors are almost indistiguishable contrast-wise.
If we do the same trick on a proper contrasting colors, the result speaks for itself:

![AAA](proper.png)

This clearly shows, that choosing the colors with the right contrast ratio is key for readability.
I took the [WCAG 2.0](https://www.w3.org/TR/UNDERSTANDING-WCAG20/visual-audio-contrast-contrast.html) color contrast ratio guitelines as basis.
Using the contrast checker tools I have picked a palette where every feature/foreground color has a __AAA__ contrast ratio to any background color.
